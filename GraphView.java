/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphs;

import java.awt.Color;
import java.awt.Graphics; 
import java.awt.Graphics2D; 
import java.awt.RenderingHints; 
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author valeria
 */
public class GraphView extends JPanel {
    
    private List<Point> points = null;
    
    private double xOffset = 0;
    private double yOffset = 0;
    
    private static double WIDTH = 470;
    private static double HEIGHT = 450;
    
    private double SCALE_X = 20; 
    private double SCALE_Y = 15; 
    
    private int number=1;
    
    private boolean isHolding = false;
    private boolean die = true;
    
    public GraphView()
        {//super();
        }
    
    public void draw()
        {this.repaint();
        }
    
    @Override 
    public void paint(Graphics grphcs)
        {super.paint(grphcs); 
        Graphics2D g2 = (Graphics2D) grphcs; 
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); 
        if (die) { 
            drawAxis(g2); 
            }
        else {
            drawAxis(g2); 
            drawGraph(g2); 
            }
        } 
    
    private void drawAxis(Graphics2D g2)
        {Color c = g2.getColor(); // save color 
        final int W = getWidth(); 
        final int H = getHeight(); 
        g2.setColor(Color.RED); 
        g2.drawLine(0, 0, 0, H); 
        g2.drawLine(0, 0, 3, 5); 
        g2.drawLine(0, 0, -3, 5); 
        int y = H / 2; 
        g2.drawLine(0, y, W, y); 
        g2.drawLine(W, y, W - 5, y - 3); 
        g2.drawLine(W, y, W - 5, y + 3); 
        g2.setColor(c); // restore color 
        } 

    public void dieGraph(Boolean die)
        {this.die = die; 
        repaint(); 
        }

    private void drawGraph(Graphics2D g2)
        {double lastX = 0; 
         double lastY = GraphView.HEIGHT / 2; 
        
        for (int x = 1; x <= GraphView.WIDTH; x++)
            {double y = GraphView.HEIGHT / 2 - func(x / SCALE_X) * SCALE_Y; 
            g2.drawLine((int)lastX, (int)lastY, (int)x, (int)y); 
            lastX = x; 
            lastY = y;
            } 
        }

        void setOffset (double xOffset, double yOffset)
            {this.xOffset = xOffset;
             this.yOffset = yOffset;
            }
        
        void setScale (double scaleX, double scaleY)
            {this.SCALE_X = scaleX;
             this.SCALE_Y = scaleY;
            }
        
        void setNumber (int number)
            {this.number=number+1;}

        void hold (boolean isHolding)
            {this.isHolding = isHolding;
            }
        
        
        private double func(double x) { 
            switch (number)
                {case (1):
                    {return 10 * Math.cos(3 * x); 
                    }
                case (2):
                    {return 10 * Math.exp(-x / 4); 
                    }
                case (3):
                    {return -10 * Math.exp(-x / 4);
                    }
                default:
                    return x;
            }
        }
            

    
}
