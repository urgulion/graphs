/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphs;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author valeria
 */
public class Graph {
    List<Point> points = new ArrayList<>();
    
    public Graph(List<Point> points)
        {this.points = points;
        }
    
    public List<Point> getPoints()
        {return points;}
    
}
